class CellsBfmDmIvGetTransactions extends Polymer.Element {

  static get is() {
    return 'cells-bfm-dm-iv-get-transactions';
  }

  static get properties() {
    return {
      endpoint: String,
      headers: {
        type: String,
        value: () => ({})
      },
      host: {
        type: String,
        value: () => ''
      },
      params:{
        type: Object,
        value: () => ({
          skip:0,
          limit:40
        })
      }
    };
  }
  /**
   * Function to handle service success response
   * @param {*} response
   */
  _successResponse(response) {
    if (response.detail.response.length) {

        this._manageDataTransactions(response.detail.response);

    }

    if (!response.detail.response.length) {
        this.$.dpGetTransactions.generateRequest();
      this.dispatchEvent(new CustomEvent('missing-transactions'));
    }

    this.dispatchEvent(new CustomEvent('all-transactions', {detail: response.detail.response}));
  }
  _manageDataTransactions(transactions){
    var transactionsTable = [];
    // transactions.forEach((trstn)=>{
    //   transactionsTable.push({
    //     type: (trstn.amount <= 0) ? 'secondary' : 'primary',
    //     icon: 'coronita:ticket',
    //     date: '18 Sep',
    //     year: '2019',
    //     description: trstn.description,
    //     tags: [
    //       {
    //         text: 'Facturas CFDI',
    //         icon: 'coronita:bank',
    //         class: 'primary'
    //       },
    //       {
    //         text: 'Uncategorized',
    //         icon: 'coronita:sales',
    //         class: 'secondary'
    //       }
    //     ],
    //     mainAmount: {
    //       amount: trstn.amount,
    //       currency: trstn.currency
    //     },
    //     subAmount: {
    //       amount: trstn.amount,
    //       currency: trstn.currency
    //     }
    //   })
    // });

    this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: true}))
    //this.dispatchEvent(new CustomEvent('transactions-table-data',{detail:transactionsTable}))
    this.dispatchEvent(new CustomEvent('transactions-table-data',{detail:transactions}))
  }
  /**
   * Function to generate enpoint and call service
   */
  starterMethod(id_credential = '') {
    this._buildEndpoint();
    this.params['id_credential'] = id_credential;
    if (this.endpoint && id_credential) {
      this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: false}))
      this.$.dpGetTransactions.generateRequest();
    }
  }
  filter(params){

  }
  /**
   * function to buil enpoint url
   */
  _buildEndpoint() {
    let host = this._getHost();
    let isMock = this._getMocks();
    this.set('endpoint', `${host}/transactions${isMock}`);
  }
  _getMocks(){
    return (window.AppConfig && window.AppConfig.mocks) ? '.json' : '';
  }
  /**
   * Function to get host from config file
   */
  _getHost() {
    return (window.AppConfig && window.AppConfig.host) ? window.AppConfig.host : this.host;
  }
  /**
   * Function to hanlde error status response
   * @param {*} reponse
   */
  _handlerError(reponse) {
    var msj = 'Estamos teniendo dificultades técnicas, favor de intentar mas tarde';
    try {
      var errorCode = reponse.detail.code;
      switch (errorCode) {
        case 401:
          this.dispatchEvent(new CustomEvent('unauthorized-error', { detail: reponse.detail.code }));
          break;
        default:
          msj = reponse.detail['error-message'] || errorCode || msj;
          break;
      }
    } catch (e) {
      //No aplicable catch handler, use default values from eventError and msj
    }
    this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: true}))
    this.dispatchEvent(new CustomEvent('account-detail', {detail: []}));
    this.dispatchEvent(new CustomEvent('error-service', { detail: msj }));
  }
}

customElements.define(CellsBfmDmIvGetTransactions.is, CellsBfmDmIvGetTransactions);
